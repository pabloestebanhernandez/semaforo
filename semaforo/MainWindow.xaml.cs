﻿using semaforo.ui;
using System;
using System.Windows.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace semaforo
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {




        public MainWindow()
        {
            InitializeComponent();

            Semaforo semaforoAv = new Semaforo(Red: RED_1, Yellow : YELLOW_1 , Green: GREEN_1);
            Semaforo semaforoCalle = new Semaforo(Red: RED_2, Yellow : YELLOW_2, Green: GREEN_2);
          
            HandlerTraficLight handler = new HandlerTraficLight(semaforoAv, semaforoCalle);

        }


       


        private void Button_Click(object sender, RoutedEventArgs e)
        {
        
        }



    }
}
