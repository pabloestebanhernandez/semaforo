﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace semaforo.ui
{
    public static class HandlerColor
    {


        private static Brush ConvertStringToBrush(string colorCode)
        {
            var converter = new System.Windows.Media.BrushConverter();
            var brush = (Brush)converter.ConvertFromString(colorCode);
            return brush;
        }


        public static Brush RedTurnOff()
        {
            return ConvertStringToBrush("#EF9A9A");  
        }

        public static Brush RedTurnOn()
        {
            return ConvertStringToBrush("#F44336");
        }



        public static Brush YellowTurnOn()
        {
            return ConvertStringToBrush("#FFEB3B");
        }

        public static Brush YellowTurnOff()
        {
            return ConvertStringToBrush("#FFF59D");

        }


        public static Brush GreenTurnOn()
        {
            return ConvertStringToBrush("#4CAF50");

        }
        public static Brush GreenTurnOff()
        {
            return ConvertStringToBrush("#A5D6A7");

        }


    }
}