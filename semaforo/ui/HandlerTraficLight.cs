﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace semaforo.ui
{
    public class HandlerTraficLight
    {

        private Timer aTimer;

        private int timeGreen, timeRed;
        private int count = 0;
        private Semaforo semaforoAv;
        private Semaforo semaforoCalle;

        public HandlerTraficLight(Semaforo semaforoAv, Semaforo semaforoCalle)
        {
            this.semaforoAv = semaforoAv;
            this.semaforoCalle = semaforoCalle;

            timeRed = 2000;
            timeGreen = 2000;


            SetTimer();
        }

        private void SetTimer()
        {
            aTimer = new Timer(2000);
            aTimer.Elapsed += OnTimedEvent;
            aTimer.AutoReset = true;
            aTimer.Enabled = true;
        }


        private  void OnTimedEvent(Object source, ElapsedEventArgs e)
        {

            count++;

            switch (count)
            {
                case 1:

                    semaforoAv.Allow();
                    semaforoCalle.Stop();

                    break;
                case 2:
                    semaforoAv.Pending();
                    break;
                case 3:
                    semaforoAv.Stop();
                    semaforoCalle.Allow();
                    break;

            }





        }

     
    }
}
