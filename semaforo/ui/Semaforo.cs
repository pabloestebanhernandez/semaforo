﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Media;
using System.Windows.Shapes;

namespace semaforo.ui
{
    public class Semaforo
    {

        private Ellipse Red;
        private Ellipse Yellow;
        private Ellipse Green;

     

        public Semaforo( Ellipse Red,  Ellipse Yellow,  Ellipse Green)
        {
            this.Red = Red;
            this.Yellow = Yellow;
            this.Green = Green;
            
            Initialize();
        }


        public void Initialize()
        {
            Red.Fill = HandlerColor.RedTurnOff();
            Yellow.Fill = HandlerColor.YellowTurnOff();
            Green.Fill = HandlerColor.GreenTurnOff();
            
        }

        public void Allow()
        {

            Red.Fill = HandlerColor.RedTurnOff();
            Yellow.Fill = HandlerColor.YellowTurnOff();
            Green.Fill = HandlerColor.GreenTurnOn();
        }

        public void Pending()
        {
            Red.Fill = HandlerColor.RedTurnOff();
            Yellow.Fill = HandlerColor.YellowTurnOn();
            Green.Fill = HandlerColor.GreenTurnOff();
        }

        public void Stop()
        {
            Red.Fill = HandlerColor.RedTurnOn();
            Yellow.Fill = HandlerColor.YellowTurnOff();
            Green.Fill = HandlerColor.GreenTurnOff();
        }

    }
}
